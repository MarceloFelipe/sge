package br.com.sge.controller;

import br.com.sge.entidade.Local;
import br.com.sge.util.Mensagem;
import br.com.sge.dao.*;

import javax.faces.bean.*;
import javax.ejb.*;
import java.util.*;

@ManagedBean
public class LocalController {
	
	private Local local;
	
	@EJB
	private LocalDAO localdao;
	
	public LocalController(){
		this.local = new Local();
	}
	
	public void salvar(){
		String erro = localdao.salvar(local);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.local = new Local(); //Limpar os campos
		}	
	}
	
	public void excluir(Local local){
		String erro = localdao.excluir(local.getIdLocal());				
	
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Exclu�do com sucesso.");
		}
	}
	
	public void editar(Local localEditado){
		this.local = localEditado;
	}
	
	public List<Local> consultar(){
		return localdao.todos();
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}
}
