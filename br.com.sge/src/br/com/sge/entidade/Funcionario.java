package br.com.sge.entidade;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
public class Funcionario {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idFuncionario;
	
	@NotNull(message = "Informe a descri��o!")
    private String descricao;
	
	@NotNull(message = "Infrome o Nome!")
	private String nome;
	
	@NotNull(message = "Infrome o CPF!")
	private String cpf;
	
	@NotNull(message = "Infrome o RG!")
	private String rg;
	
	@NotNull(message = "Infrome o numero do telefone!")
	private String telefone;
	
	@NotNull(message = "Infrome o Sexo!")
	private String sexo;
	
	private String email;
	
	@NotNull(message = "Infrome a cidade!")
	private String cidade;
	
	@NotNull(message = "Infrome o nome da rua!")
	private String rua;
	
	@NotNull(message = "Infrome o numero da casa!")
	private String numero;
	
	@NotNull(message = "Infrome o complemento!")
	private String complemeto;
	
	private String cep;

	public int getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(int idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemeto() {
		return complemeto;
	}

	public void setComplemeto(String complemeto) {
		this.complemeto = complemeto;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	
	
	
	
}
