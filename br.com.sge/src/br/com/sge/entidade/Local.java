package br.com.sge.entidade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity

public class Local {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idLocal;
	
	@NotNull(message = "Informe a descri��o!")
	private String descricao;
	
	
	//Mapeamento OneToOne de Local para Adcioanis
	//@OneToOne(mappedBy = "local")
	//private Adcionais adcionais;
	
	//Mapeamento OndeToOne de Local para Evento
	@OneToOne(mappedBy = "local")
	private Evento evento;

	
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIdLocal() {
		return idLocal;
	}

	public void setIdLocal(Integer idLocal) {
		this.idLocal = idLocal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idLocal == null) ? 0 : idLocal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Local other = (Local) obj;
		if (idLocal == null) {
			if (other.idLocal != null)
				return false;
		} else if (!idLocal.equals(other.idLocal))
			return false;
		return true;
	}		
}
