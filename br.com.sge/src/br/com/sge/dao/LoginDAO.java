package br.com.sge.dao;

import java.io.Serializable;
import javax.ejb.*;
import javax.persistence.TypedQuery;

import br.com.sge.entidade.Login;
import br.com.sge.dao.GenericDAO;

@Stateless
public class LoginDAO extends GenericDAO<Login> {
	
	public LoginDAO(){
		super(Login.class);
	}
	
	public String todos(Serializable usuario, Serializable senha){
		
		TypedQuery<Login> query= em.createQuery
        ("select x from Login as x where x.usuario like '"+usuario+"' and x.senha like '"+senha+"'",Login.class);
		
			if(query.getResultList().size() == 1){
				
				return query.getResultList().get(0).getNome();

			}else{		
				return null;
			}	
	}
}
