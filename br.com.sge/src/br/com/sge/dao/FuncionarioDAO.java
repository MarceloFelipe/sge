package br.com.sge.dao;

import javax.ejb.Stateless;

import br.com.sge.entidade.*;

@Stateless
public class FuncionarioDAO extends GenericDAO<Funcionario>{
	
	public FuncionarioDAO(){
		super(Funcionario.class);
	}
}
