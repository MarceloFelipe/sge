package br.com.sge.entidade;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "casamento")
@PrimaryKeyJoinColumn(name ="idEvento")
@DiscriminatorValue("C")
public class Casamento extends Evento{
	
	@NotNull(message="Informe o nome do noivo!")
	private String nomeNoivo;
	
	@NotNull(message="Informe o nome da noiva!")
	private String nomeNoiva;
	
	
	public String getNomeNoivo() {
		return nomeNoivo;
	}

	public void setNomeNoivo(String nomeNoivo) {
		this.nomeNoivo = nomeNoivo;
	}

	public String getNomeNoiva() {
		return nomeNoiva;
	}

	public void setNomeNoiva(String nomeNoiva) {
		this.nomeNoiva = nomeNoiva;
	}	
}
