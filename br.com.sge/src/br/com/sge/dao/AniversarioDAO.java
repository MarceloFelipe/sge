package br.com.sge.dao;

import javax.ejb.Stateless;

import br.com.sge.entidade.Aniversario;

@Stateless
public class AniversarioDAO extends GenericDAO<Aniversario> {
	
	public AniversarioDAO(){
		super(Aniversario.class);
	}
}
