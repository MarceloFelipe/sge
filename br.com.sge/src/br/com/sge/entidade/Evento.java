package br.com.sge.entidade;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "evento")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "tipo",discriminatorType = DiscriminatorType.CHAR)
public class Evento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEvento;
	
	@Temporal(TemporalType.DATE)
	private Date data;
	
	@Temporal(TemporalType.TIME)
	private Date horario;
	
	@NotNull(message = "Informe o numero de convidados!")
	private int numeroConvidado;
	
	private int ticket;
	
	private int convite;
	
	private int cardapio;
	
	//Mapeamento OneToOne de Evento para Local
	@OneToOne
	@JoinColumn(name = "idLocal")
	private Local local;

	
	
	public int getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(int idEvento) {
		this.idEvento = idEvento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getHorario() {
		return horario;
	}

	public void setHorario(Date horario) {
		this.horario = horario;
	}

	public int getNumeroConvidado() {
		return numeroConvidado;
	}

	public void setNumeroConvidado(int numeroConvidado) {
		this.numeroConvidado = numeroConvidado;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public int getTicket() {
		return ticket;
	}

	public void setTicket(int ticket) {
		this.ticket = ticket;
	}

	public int getConvite() {
		return convite;
	}

	public void setConvite(int convite) {
		this.convite = convite;
	}

	public int getCardapio() {
		return cardapio;
	}

	public void setCardapio(int cardapio) {
		this.cardapio = cardapio;
	}

	
}
