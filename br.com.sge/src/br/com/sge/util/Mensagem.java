package br.com.sge.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class Mensagem {
	
	public static void sucesso(String msg){
		FacesContext
			.getCurrentInstance()
			.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null));
	}
	
	public static void erro(String msg){
		FacesContext
			.getCurrentInstance()
			.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));
	}
}
