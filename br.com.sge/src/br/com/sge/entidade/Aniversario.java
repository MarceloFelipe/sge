package br.com.sge.entidade;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "aniversario")
@PrimaryKeyJoinColumn(name ="idEvento")
@DiscriminatorValue("A")
public class Aniversario extends Evento{
	
	/*@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idAniversario;*/
	
	@NotNull(message = "Informe oo nome da aniversariante!")
	private String nomeAniversariante;

	public String getNomeAniversariante() {
		return nomeAniversariante;
	}

	public void setNomeAniversariante(String nomeAniversariante) {
		this.nomeAniversariante = nomeAniversariante;
	}	
}
