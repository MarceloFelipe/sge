package br.com.sge.controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.sge.entidade.Login;
import br.com.sge.util.Mensagem;
import br.com.sge.dao.LoginDAO;

@ManagedBean
public class LoginController {
	
	private Login login;
	
	@EJB
	private LoginDAO logindao;

	public LoginController(){
		this.login = new Login();
	}
	
	public String consultar(){
		
		 String nome = logindao.todos(login.getUsuario(),login.getSenha());

		 if(nome == null){
			Mensagem.erro("Usu�rio ou Senha incorreto ");
			this.login = new Login();
			return null;
		}
		else{
			 if(nome.equals("admin") ){
				 
					return  "administrativo.xhtml";
					
				}else{
					
					return "index.xhtml";
					
				}
		}
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}
}
