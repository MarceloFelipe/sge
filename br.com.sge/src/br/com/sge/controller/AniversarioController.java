package br.com.sge.controller;

import br.com.sge.entidade.*;
import br.com.sge.dao.*;
import br.com.sge.util.*;

import javax.faces.bean.*;
import javax.ejb.*;
import java.util.*;

@ManagedBean
public class AniversarioController {

	private Aniversario aniversario;
	
	@EJB
	private AniversarioDAO aniversariodao;
	

	
	public AniversarioController(){
		this.aniversario = new Aniversario();
	}
	
	public void salvar(){
		String erro = aniversariodao.salvar(aniversario);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.aniversario = new Aniversario(); //Limpar os campos
		}	
	}
	
	public void editar(Aniversario aniversarioEditado){
		this.aniversario = aniversarioEditado;
	}
	
	public List<Aniversario> consultar(){
		return aniversariodao.todos();
	}

	public Aniversario getAniversario() {
		return aniversario;
	}

	public void setAniversario(Aniversario aniversario) {
		this.aniversario = aniversario;
	}
}
