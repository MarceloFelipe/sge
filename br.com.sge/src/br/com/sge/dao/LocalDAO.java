package br.com.sge.dao;

import javax.ejb.Stateless;

import br.com.sge.entidade.Local;

@Stateless
public class LocalDAO extends GenericDAO<Local> {
	
	public LocalDAO(){
		super(Local.class);
	}
}
