package br.com.sge.controller;

import br.com.sge.entidade.*;
import br.com.sge.dao.*;
import br.com.sge.util.*;

import javax.faces.bean.*;
import javax.ejb.*;
import java.util.*;

@ManagedBean
public class CasamentoController {

	private Casamento casamento;
	
	@EJB
	private CasamentoDAO casamentodao;
	
	
	public CasamentoController(){
		this.casamento = new Casamento();
	}
	
	public void salvar(){
		String erro = casamentodao.salvar(casamento);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.casamento = new Casamento(); //Limpar os campos
		}	
	}
	
	public void editar(Casamento casamentoEditado){
		this.casamento = casamentoEditado;
	}
	
	public List<Casamento> consultar(){
		return casamentodao.todos();
	}

	public Casamento getCasamento() {
		return casamento;
	}

	public void setCasamento(Casamento casamento) {
		this.casamento = casamento;
	}

	
}
