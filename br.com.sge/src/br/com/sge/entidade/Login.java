package br.com.sge.entidade;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Login {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idLogin;
	
	@NotNull(message = "Informe o usuario!")
	private String usuario;

	@NotNull(message = "Informe a senha!")
	private String senha;
	
	private String nome;
	
	public int getIdLogin() {
		return idLogin;
	}

	public void setIdLogin(int idLogin) {
		this.idLogin = idLogin;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
