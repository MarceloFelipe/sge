package br.com.sge.controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.sge.dao.FuncionarioDAO;
import br.com.sge.entidade.*;
import br.com.sge.util.*;

@ManagedBean
public class FuncionarioController {

	private Funcionario funcionario;
	
	@EJB
	private FuncionarioDAO funcionariodao;
	
	public FuncionarioController(){
		this.funcionario = new Funcionario();
	}
	
	public void salvar(){
		String erro = funcionariodao.salvar(funcionario);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.funcionario = new Funcionario(); //Limpar os campos
		}	
	}
	
	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
}
