package br.com.sge.dao;

import br.com.sge.entidade.*;

import javax.ejb.*;

@Stateless
public class CasamentoDAO extends GenericDAO<Casamento>{
	
	public CasamentoDAO(){
		super(Casamento.class);
	}

}
